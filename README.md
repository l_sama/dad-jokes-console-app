# DadJokeApp
DadJokeApp is an application that allows print dad jokes.

## This application named DadJokeApp  allows:
* Print all full jokes
* Print random joke
* Print chosen joke
* Print count of stored jokes
* Print mix of random question
* Add new jokes and punchlines

## Setup 
Git clone the repository URL on the command line or using tools like [Sourcetree](https://www.sourcetreeapp.com/). Alternatively download the repository and unzip the file to you desired location.

## Build and run the app
If you have Visual Studio 2017 or 2019 installed

  1. Open the sample solution in Visual Studio.
 2. Press F5 to build and run the app. This will open the console application.
3. Follow the instructions displayed in console application menu.
	
Alternatively 
	
1. Open the sample solution in File Explorer. 
2. Navigate to folder that contains the file DadJokeApp.csproj.
3. Right click the folder and select option to open folder in command line (select “Open Command Window Here”).
4. Type in `dotnet run` and press enter to run the application.
5. Follow the instructions displayed in console application menu.
	
