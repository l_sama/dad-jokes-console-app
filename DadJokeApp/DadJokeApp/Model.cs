﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DadJokeApp
{
    public class Model
    {
        List<Joke> jokes = new List<Joke>() {
        new Joke() { Number = 1, Question = "Dad, did you get a haircut ?", Punchline = "No, I got them all cut!" },
        new Joke() { Number = 2, Question = "How do you get a squirrel to like you?", Punchline = "Act like a nut." },
        new Joke() { Number = 3, Question = "Why don't eggs tell jokes?", Punchline = "They'd crack each other up." },
        new Joke() { Number = 4, Question = "What do you call someone with no body and no nose?", Punchline = "Nobody knows."},
        new Joke() { Number = 5, Question = "Why couldn't the bicycle stand up by itself?", Punchline = "It was two tired." },
        new Joke() { Number = 6, Question = "What kind of shoes do ninjas wear?", Punchline = "Sneakers!" },
        new Joke() { Number = 7, Question = "How does a penguin build its house?", Punchline = "Igloos it together."},
        new Joke() { Number = 8, Question = "Why did the math book look so sad?", Punchline = "Because of all of its problems!"  },
        new Joke() { Number = 9, Question = "How many tickles does it take to make an octopus laugh?", Punchline = "Ten tickles." },
        new Joke() { Number = 10, Question = "Did you hear about the guy who invented the knock-knock joke?", Punchline = "He won the 'no-bell' prize." }
        };

        public void PrintAllJokes()
        {
            Console.WriteLine("All Jokes:");

            foreach (Joke joke in jokes)
            {
                Console.WriteLine(joke.ToString());
            }

            Console.WriteLine("Enter any key to exit to menu...!");
            Console.ReadKey();
        }
        public void PrintRandomJoke()
        {
            Console.WriteLine("Random joke:");
            var random = new Random();
            var randomJokeIndex = random.Next(jokes.Count);

            Console.WriteLine(jokes[randomJokeIndex].ToString());
            Console.WriteLine("Enter any key to exit to menu...!");
            Console.ReadKey();
        }


        public void PrintNumberOfJokes()
        {
            int numberOfJokes = jokes.Count;
            Console.WriteLine("Total number of Jokes:" + numberOfJokes);
            Console.WriteLine("Enter any key to exit to menu...!");
            Console.ReadKey();
        }



        public void PrintSelectedJoke()
        {
            Console.WriteLine("Please select and print one of these available jokes:");

            foreach (Joke joke in jokes)
            {
                Console.Write(joke.Number + " ");
            }

            try
            {
                int selectedJokeIndex = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Your selected joke:");
                Console.WriteLine(jokes[selectedJokeIndex - 1].ToString());
                Console.WriteLine("Enter any key to exit to menu...!");
                Console.ReadKey();
            }
            catch (Exception)
            {

                Console.WriteLine("Only values from selection option are accepted");
                Console.WriteLine("Enter any key to exit to menu...!");
                Console.ReadKey();
            }
        }

        public void PrintMix()
        {
            var random1 = new Random();
            var random2 = new Random();
            var randomJokeIndex1 = random1.Next(jokes.Count);
            var randomJokeIndex2 = random2.Next(jokes.Count);

            if (randomJokeIndex1 == randomJokeIndex2)
            {
                PrintMix();
            }
            else
            {
                Console.WriteLine("Random question: " + jokes[randomJokeIndex1].Question + " Random punchline: " + jokes[randomJokeIndex2].Punchline);
                Console.WriteLine("Enter any key to exit to menu...!");
                Console.ReadKey();
            }
        }

        public void AddNewJoke()
        {

            int maxNumber = jokes.Max(n => n.Number);
            int number = maxNumber + 1;

            string inappWord = "abc";

            Console.WriteLine("Please enter the question");
            string question = Console.ReadLine();

            Console.WriteLine("Please enter the punchline");
            string punchline = Console.ReadLine();

            if (question.Contains(inappWord) || punchline.Contains(inappWord))
            {
                Console.WriteLine("The joke contains inappropriate word");
                Console.WriteLine("Enter any key to exit to menu...!");
                Console.ReadKey();

            }
            else
            {


                jokes.Add(new Joke { Number = number, Question = question, Punchline = punchline });

                Console.WriteLine("Added  question number is: " + number);
                Console.WriteLine("Enter any key to exit to menu...!");
                Console.ReadKey();
            }
        }
    }
}
