﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DadJokeApp
{
    public class Joke
    {
        public int Number { get; set; }
        public string Question { get; set; }
        public string Punchline { get; set; }

        public override string ToString()
        {
            return $"Number: {Number} Question: {Question} Punchline: {Punchline}";
        }
    }
}
