﻿using System;

namespace DadJokeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Model model = new Model();

            static void PrintMenu()
            {
                Console.WriteLine("Dad Joke Application");
                Console.WriteLine("------------------------");
                Console.WriteLine("Choose an option from the following list:");
                Console.WriteLine("Type 1 to print all jokes, and then press Enter");
                Console.WriteLine("Type 2 to print random joke, and then press Enter");
                Console.WriteLine("Type 3 to select particular joke, and then press Enter");
                Console.WriteLine("Type 4 to print a number of saved jokes, and then press Enter");
                Console.WriteLine("Type 5 to print a mix of random jokes and punchlines, and then press Enter");
                Console.WriteLine("Type 6 to to add your joke and punchline, and then press Enter");
                Console.WriteLine("Type q if you want to quit, and then press Enter ");
                Console.WriteLine("------------------------");
                Console.Write("Your selection:");
            }

            PrintMenu();

            while (true)
            {
                switch (Console.ReadLine())
                {
                    case "1":
                        model.PrintAllJokes();
                        break;
                    case "2":
                        model.PrintRandomJoke();
                        break;
                    case "3":
                        model.PrintSelectedJoke();
                        break;
                    case "4":
                        model.PrintNumberOfJokes();
                        break;
                    case "5":
                        model.PrintMix();
                        break;
                    case "6":
                        model.AddNewJoke();
                        break;
                    case "q":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.Clear();
                        PrintMenu();
                        break;
                }
            }

        }
    }
}


